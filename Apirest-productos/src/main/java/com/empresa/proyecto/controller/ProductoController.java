package com.empresa.proyecto.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.empresa.proyecto.dominio.model.Producto;
import com.empresa.proyecto.dominio.puerto.repository.ProductoRepository;
 
 

@RestController
@CrossOrigin(value="*")
@RequestMapping("productos")
public class ProductoController {


	// inyeccion de dependencias, es posible hacerlo por medio del constructor de
		// esta manera
		// es posible hacer los test de manera mas adecuada
	
		
	
		@Autowired
		private ProductoRepository productoRepository;

		@GetMapping("/health")
		public String healthCheck() { 
			 
			return "runing ok..";
		}

		
		@GetMapping
		public ResponseEntity<List<Producto>> getProducto() {
			List<Producto> producto = productoRepository.findAll();
			/*
			 * codigo hardcodeado Producto producto = new Producto();
			 * producto.setCodigo(1234); producto.setNombre("computador"); return
			 * ResponseEntity.ok(producto);
			 */
			return ResponseEntity.ok(producto);
		}

		@RequestMapping(value = "/{id}", method = RequestMethod.GET)
		public ResponseEntity<Producto> getProducto(@PathVariable("id") Long codigoProducto) {
			Optional<Producto> optionalProducto = productoRepository.findById(codigoProducto);
			if (optionalProducto.isPresent()) {
				return ResponseEntity.ok(optionalProducto.get());
			} else {
				return ResponseEntity.noContent().build();
			}

		}

		@PostMapping("/create")
		public ResponseEntity<?> createProducto(@RequestBody Producto producto) {
			try {
				Producto nuevoProducto = productoRepository.save(producto);
				return ResponseEntity.status(HttpStatus.OK).body("Se agrego el producto: "+nuevoProducto.getNombre());
			} catch (Exception e) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
			}  
		}

		@DeleteMapping(value = "/{id}")
		public ResponseEntity<?> deleteProducto(@PathVariable("id") Long codigoProducto) {
			try {

				productoRepository.deleteById(codigoProducto);
				return ResponseEntity.status(HttpStatus.NO_CONTENT).body("eliminado");

			} catch (Exception e) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
			}
		}

		@PutMapping("/update")
		public ResponseEntity<Producto> updateProducto(@RequestBody Producto producto) {
			Optional<Producto> optionalProducto = productoRepository.findById(producto.getCodigo());
			if (optionalProducto.isPresent()) {
				Producto updateProducto = optionalProducto.get();
				updateProducto.setNombre(updateProducto.getNombre());
				productoRepository.save(updateProducto);
				return ResponseEntity.ok(optionalProducto.get());
			} else {
				return ResponseEntity.noContent().build();
			} 
		}
		/*
		 * estas son dos formas en las que puedo consumir servicio y dan el mismo
		 * resultado, la primera @GetMapping me dice que el servicio se consume desde la
		 * raiz, en la segunda indicamos la url mediante por la cual lo
		 * consumiremos @RequestMapping(value="hello",method=RequestMethod.GET)
		 */


}
